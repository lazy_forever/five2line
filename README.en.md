# Based on Qt (back-end using Golang + MySQL), a Gobang game program

#### Description
This program is a Gobang game based on Qt, with Golang + MySQL used as  the back-end. It was the final project of the author's advanced  programming course in their freshman year.

#### Software Architecture
Qt Creator 9.0.0、GoLand、Xshell（Used for configuring servers.）

#### Project structure

![项目结构](images/项目结构.png)

#### Overall process

![整体流程](images/整体流程.png)

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Optimization objectives

1.  Improve class encapsulation to avoid directly exposing class members.
2.  Improve class destructors to prevent serious memory leaks.
3.  Improve server-side code to avoid overuse of GET for client-server interaction, and optimize the design approach for listening to each other's signals.
4.  Add a personal score field and a ranking list feature.
5.  Add the ability to know the opponent's name during the game and enable communication during the game.
6.  Add the ability to have multiple games running simultaneously.
