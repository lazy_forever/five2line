#ifndef PVESCENE_H
#define PVESCENE_H

#include "myfatherscene.h"
#include "mybutton.h"

class pveScene : public myFatherScene
{
    Q_OBJECT
public:
    pveScene();

    int aiFlag=0;

    mybutton * peopleFirstBtn=new mybutton(":/images/PlayerFirst.png");
    mybutton * computerFirstBtn=new mybutton(":/images/ComputerFirst.png");
    mybutton * againBtn=new mybutton(":/images/AgainBegin.png");

    void end();

    void ai();

    int chessType(int rotate, int i, int j);

    int value(int i, int j);

    void moveByOne(int rotate, int *i, int *j);

    //重写鼠标释放函数
    void mouseReleaseEvent(QMouseEvent *event);

    //重新开始的函数
    void constructor();

signals:

};

#endif // PVESCENE_H
