#ifndef PVPONLINE_H
#define PVPONLINE_H

#include "myfatherscene.h"
#include "online.h"
#include <QLabel>


class pvponline :  public myFatherScene
{
    Q_OBJECT
public:
    pvponline(online * o,QString mac);
    QString mac;
    online * o;


    bool myFlag=false;

    mybutton * whiteBtn=new mybutton(":/images/BWhite.png");
    mybutton * blackBtn=new mybutton(":/images/BBlack.png");
    mybutton * fallBtn=new mybutton(":/images/Fall.png");
    QLabel * label;

    void end();
    void wait();
    void onlineupdate();
    void exit();



    bool chooseflag(int flag);
    void constructor();


    //重写鼠标释放函数
    void mouseReleaseEvent(QMouseEvent *event);
    //重写关闭窗口函数
    void closeEvent(QCloseEvent *event);

private:
    bool waiting=true;

signals:
    void back();
};

#endif // PVPONLINE_H
