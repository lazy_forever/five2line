#ifndef MYBUTTON_H
#define MYBUTTON_H

#include <QPushButton>

class mybutton : public QPushButton
{
    Q_OBJECT
public:
    mybutton(QString image);

    //图片地址
    QString img;

    //弹跳特效
    void zoomUp();
    void zoomDown();

signals:

};

#endif // MYBUTTON_H
