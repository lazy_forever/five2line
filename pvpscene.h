#ifndef PVPSCENE_H
#define PVPSCENE_H

#include "myfatherscene.h"
#include <QLabel>

class pvpScene : public myFatherScene
{
    Q_OBJECT
public:
    pvpScene();

    mybutton * startBtn=new mybutton(":/images/BeginGame.png");
    mybutton * againBtn=new mybutton(":/images/AgainBegin.png");


    QLabel * label;
    void end();

    //重写鼠标释放函数
    void mouseReleaseEvent(QMouseEvent *event);

    //重新开始的函数
    void constructor();

signals:

};

#endif // PVPSCENE_H
